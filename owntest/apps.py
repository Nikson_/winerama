from django.apps import AppConfig


class OwntestConfig(AppConfig):
    name = 'owntest'
