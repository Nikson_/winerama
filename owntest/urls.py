from django.conf.urls import url

from . import views

urlpatterns = [
    # ex: /
    url(r'^$', views.index_owntest, name='index_owntest'),
]