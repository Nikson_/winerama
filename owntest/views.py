from django.shortcuts import render
from .models import Cars

# Create your views here.

def index_owntest(request):
    cars_list = Cars.objects.order_by('id')
    context = {'cars_list': cars_list}
    return render(request, 'owntest/list.html', context)